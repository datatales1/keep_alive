# Keep Alive



## Files

Download the two files to your machine into one folder. If you don't want to download them, just create empty .txt files inside the same folder, change the appendix and copy the code inside.

## Execute

To activate the keep-alive mode just double-click the .bat file. To end the keep-alive mode, close the terminal window that opened.